package xyz.abhaychauhan.cinebuff.cinebuff.activity.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by abhay on 14/05/17.
 */

public class DbContract {

    public static final String AUTHORITY = "xyz.abhaychauhan.cinebuff.cinebuff.activity";
    public static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PATH_MOVIE = "movie";

    public static final class MovieEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_URI.buildUpon().appendPath(PATH_MOVIE).build();

        public static final String TABLE_NAME = "movie";

        public static final String COLUMN_BACKDROP_PATH = "backdrop_path";
        public static final String COLUMN_POSTER_PATH = "poster_path";
        public static final String COLUMN_MOVIE_ID = "movie_id";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_SYNOPSIS = "synopsis";
        public static final String COLUMN_RELEASE_DATE = "release_date";
        public static final String COLUMN_RUNTIME = "runtime";
        public static final String COLUMN_GENRES = "genres";
        public static final String COLUMN_LANGUAGES = "languages";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_RATING = "rating";
        public static final String COLUMN_VOTE = "vote";
        public static final String COLUMN_TAGLINE = "tagline";
    }

}
