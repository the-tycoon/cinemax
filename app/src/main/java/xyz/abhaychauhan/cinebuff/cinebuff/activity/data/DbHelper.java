package xyz.abhaychauhan.cinebuff.cinebuff.activity.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by abhay on 17/05/17.
 */

public class DbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "cinemax.db";
    public static final int DATABASE_VERSION = 1;

    private final String CREATE_MOVIE_ENTRIES = "CREATE TABLE " + DbContract.MovieEntry.TABLE_NAME +
            " { " + DbContract.MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            DbContract.MovieEntry.COLUMN_BACKDROP_PATH + " TEXT, " +
            DbContract.MovieEntry.COLUMN_POSTER_PATH + " TEXT, " +
            DbContract.MovieEntry.COLUMN_MOVIE_ID + " TEXT, " +
            DbContract.MovieEntry.COLUMN_TITLE + " TEXT, " +
            DbContract.MovieEntry.COLUMN_SYNOPSIS + " TEXT, " +
            DbContract.MovieEntry.COLUMN_RELEASE_DATE + " TEXT, " +
            DbContract.MovieEntry.COLUMN_RUNTIME + " TEXT, " +
            DbContract.MovieEntry.COLUMN_GENRES + " TEXT, " +
            DbContract.MovieEntry.COLUMN_LANGUAGES + " TEXT, " +
            DbContract.MovieEntry.COLUMN_STATUS + " TEXT, " +
            DbContract.MovieEntry.COLUMN_RATING + " TEXT, " +
            DbContract.MovieEntry.COLUMN_VOTE + " INTEGER DEFAULT 0, " +
            DbContract.MovieEntry.COLUMN_TAGLINE + " TEXT );";

    private final String DROP_MOVIE_ENTRIES = "DROP TABLE IF EXISTS " +
            DbContract.MovieEntry.TABLE_NAME;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_MOVIE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_MOVIE_ENTRIES);
        onCreate(db);
    }
}
